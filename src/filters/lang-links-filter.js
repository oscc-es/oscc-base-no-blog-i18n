module.exports = function langLinksFilter(collection, lang) {
  let langLinks = [];
  const generalCondition = (item, lang) => {
    const itemLang = item.template._dataCache.lang;

    return (
      item.fileSlug == this.page.fileSlug ||
      (this.page.fileSlug == lang && item.fileSlug == itemLang)
    );
  };

  for (let item of collection) {
    const itemLang = item.template._dataCache.lang;

    if (itemLang == lang) {
      continue;
    }

    if (generalCondition(item, lang)) {
      langLinks.push({
        lang: itemLang,
        url: item.url,
        langName: item.data.langName
      });
    }
  }

  return langLinks;
};
